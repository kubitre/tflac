const initState = {
    "elements": [
        {
            id: 1,
            name: "File",
            elementsInline: [
                {
                    id: 10,
                    name: "New File",
                    action:"create",
                },
                {
                    id: 11,
                    name: "New Window",
                    action:"new",
                },
                {
                    id: 12,
                    name: "Open File",
                    action: "open"
                },
                {
                    id: 13,
                    name: "Save File",
                    action:"save",
                },
                {
                    id: 14,
                    name: "Exit",
                    action: "exit"
                }
            ]
        },
        {
            id: 2,
            name: "Text",
            elementsInline: [
                {
                    id: 21,
                    name: "Staging",
                    action: "modal",
                    params: "stag"
                },
                {
                    id: 22,
                    name: "Grammar",
                    action: "modal",
                    params: "gram"
                },
                {
                    id: 23,
                    name: "Classification",
                    action:"modal",
                    params: "class"
                },
                {
                    id: 24,
                    name: "Method Analys",
                    action:"modal",
                    params: "analys"
                },
                {
                    id: 25,
                    name: "Diagnostic And Neitrilize Errors",
                    action:"modal",
                    params: "neitr"
                },
                {
                    id: 26,
                    name: "List of Sources",
                    action: "modal",
                    params: "list_sources"
                },
                {
                    id: 27,
                    name: "Source Code",
                    action: "modal",
                    params: "open_sources"
                }
            ]
        },
        {
            id: 3,
            name: "Tests",
            elementsInline: [
                {
                    id: 31,
                    name: "First example",
                    action: "open",
                    params: "example1"
                },
                {
                    id: 32,
                    name: "Second example",
                    action: "open",
                    params: "example2"
                },
                {
                    id: 33,
                    name: "Third example",
                    action: "open",
                    params: "example3"
                },
                {
                    id: 34,
                    name: "Fourth example",
                    action: "open",
                    params: "example4"
                },
                {
                    id: 35,
                    name: "Fifth example",
                    action: "open",
                    params: "example5"
                }
            ]
        },
        {
            id: 4,
            name: "Start",
            action: "execute"
        },
        {
            id: 5,
            name: "Help",
            action: "modal",
            params: "help",
        }
    ]
}

export default function TMI_topMenu(state = initState, action){
    const {type, payload} = action;

    switch(type){
        default:
            return state;
    }
}