export const Examples = [
    {
        id: 2,
        name: "example1",
        source: "int example1\nfloat *test1;"
    },
    {
        id: 3,
        name: "example2",
        source: "int example2;\nfloat ;"
    },
    {
        id: 4,
        name: "example3",
        source: "float *test1, *test2;"
    },
    {
        id: 5,
        name: "example4",
        source: "int *test1, test2;"
    },
    {
        id: 6,
        name: "example5",
        source: "int float test;"
    }
]