const prefix = "STATUS_BAR_STATE_";

export const CHANGE_COUNTERS = prefix + "CHANGE_COUNTERS";
export const ADD_WARNINGS_AND_ERRORS = prefix + "ADD_WARNINGS_AND_ERROS";
export const ADD_LINE_COL = prefix + "ADD_LINE_COL";