const prefix = "MODAL_WINDOW_STATE_";

export const OPEN_MODAL_WINDOW = prefix + "OPEN_MODAL_WINDOW";
export const CHANGE_SOURCE_MW = prefix + "CHANGE_SOURCE_MW";
export const CLOSE_MW = prefix + "CLOSE_MODAL_WINDOW";