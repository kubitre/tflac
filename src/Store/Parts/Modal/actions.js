import { CHANGE_SOURCE_MW, OPEN_MODAL_WINDOW, CLOSE_MW } from "./constants";

export function SetModalHelp(data){
    return {
        type: CHANGE_SOURCE_MW,
        payload: {
            "head": "HELP",
            "body": `<div class="body_of_help">
            <pre>
    This is page for instruction of our application
    1. Typing your source code in input field
    2. Touch on Execute button
    3. Check output results in output block
            </pre>
            </div>`
        }
    }
}

export function SetModalClassification(){
    return {
        type: CHANGE_SOURCE_MW,
        payload: {
            "head": "Classification",
            "body": `<div class="body_of_classification">
            <pre>
    Грамматика, описанная в предыдущей главе, относится к классу
    автоматных грамматик(праволинейных). 
    Доказательство:
    <оператор перехода> -> <тип> <идентификатор> <конец предложения>
    В данном случае имеем вид продукции:
    P -> ABC, 
    где: 
    A ->a|b, 
    B -> c | dB | EB 
    C -> d
    E -> t | tE| e
    где a = int, b = float, с = цифра|буква, d = ;, t = *
    По теореме о эквивалентности правостороннего типа грамматики
    автоматной, получаем, что грамматика автоматная.        
            </pre>
            </div>
            `
        } 
    }
}

export function SetModalMethodAnalys(){
    return {
        type: CHANGE_SOURCE_MW,
        payload: {
            "head": "Method Analys",
            "body": `<div class="body_of_method_analys">
            <pre>
    На вход синтаксического анализатора подается список лексем, 
    полученный на стадии лексического анализа. Для реализации 
    автоматного распознавателя был использован паттерн 
    “Состояние”. Состояние умеет обрабатывать следующую лексему, 
    выводить все возникшие ошибки и возвращать следующее состояние.
    
    В реализации есть следующие состояния:
    InitState - состояние в момент передачи ему токенов с этапа лексического анализа.
    TypeState- состояние после обнаруженного типа.
    PointerState - состояние после обнаруженного символа “*”.
    SpaceState- состояние после обнаружения токена “Пробел”.
    ErrorState - состояние после обнаружение синтаксической ошибки.
    IdentifierState -  состояние обнаруженного идентификатора.
    EndState- состояние окончания объявления переменной.
    CommaState - состояние после обнаруженного знака “,”            
            </pre>
            </div>
            `
        }
    }
}

export function SetModalDiagnosticAndNeitralisation(){
    return {
        type: CHANGE_SOURCE_MW,
        payload: {
            "head": "Diagnostic and Neitrilize Errors",
            "body" : `<div class="body_if_diagnostic_and_neitrilize">
                <pre>
    Нейтрализация ошибок основана на методе Айронса. Так, 
    при обнаружении токена, приводящее в недопустимое состояние, 
    данная ветка будет отброшена или перестроена в зависимости 
    от контекста. При обнаружении ошибки пользователю будет 
    показано сообщение ошибке, содержащее позицию и имя 
    некорректной лексемы, а также выведена нейтрализованная 
    часть выражения.
                </pre>
            </div>
            
            `
        }
    }
}

export function SetModalListOfSources(){
    return {
        type: CHANGE_SOURCE_MW,
        payload: {
            "head": "List of sources",
            "body": `<div class="body_of_sources_list">
            <pre>
    Список источников
    1. Ахо А., Лам М., Сети Р., Ульман Дж. Компиляторы: принципы, технологии, инструменты: пер. с англ., 2-е изд. – М.: Вильямс, 2008.
    2. Шорников Ю. В. Теория и практика языковых процессов: Учеб. пособие – Новосибирск: Издательство НГТУ, 2004. – 208 с. – (Серия «Учебники НГТУ»)");
            </pre>
            </div>
            `
        }
    }
}

export function SetModalSourcesCode(){
    return {
        type: CHANGE_SOURCE_MW,
        payload: {
            "head": "Source of Codes",
            "body": `<div class="body_of_sources_codes">
            <pre>
    Исходный код анализаторов: <a href="https://gitlab.com/kubitre/tflac/tree/backend_service">Перейти</a>
    Исходный код клиентского приложения: <a href="https://gitlab.com/kubitre/tflac/tree/client_app">Перейти</a>
            </pre>
            </div>
            `
        }
    }
}

export function SetModalTask(){
    return {
        type: CHANGE_SOURCE_MW,
        payload: {
            "head": "Course Task staging",
            "body": `<div class="body_of_task_staging">
            <p>Декларирование типов переменных языка C++</p>
            </div>`
        }
    }
}


export function SetModalGrammar(){
    return {
        type: CHANGE_SOURCE_MW,
        payload: {
            "head": "Grammar definition",
            "body": `<div class="body_of_grammar" style="text-align: left;">
            <pre>
    Grammar declared:
    G[Z] = {VT, VN, Z, P}
    VT= {0, ..., 9, *, ,, int, float}
    VN= {&lt;expression&gt;, &lt;pointer&gt;, &lt;identifier&gt;, &lt;type&gt;, &lt;num&gt;, &lt;end statement&gt;}
    Z = &lt;operator transition&gt;

    P:
    &lt;operator transition&gt; -&gt; &lt;type&gt; &lt;identifier&gt; &lt;end statement&gt;
    &lt;type&gt; -&gt; int | float
    &lt;pointer&gt; -&gt; * | * &lt;pointer&gt; | e
    &lt;identifier&gt; -&gt; (A|...|Z|a|...|z|&lt;num&gt;)+|, &lt;identifier&gt;| &lt;pointer&gt; &lt;identifier&gt;
    &lt;num&gt; -&gt; (1|2|...|9|0)+ | e
    &lt;end statement&gt; -&gt; ;        
            </pre>
            </div>`
        }
    }
}


export function OpenModalWindow(){
    return {
        type: OPEN_MODAL_WINDOW,
        payload: true
    }
}

export function CloseModalWindow(){
    return {
        type: CLOSE_MW,
        payload: false,
    }
}