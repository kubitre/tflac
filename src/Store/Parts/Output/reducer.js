import { ADD_OUTPUT_BODY, CHANGE_OUTPUT_TYPE } from "./constants";

const initState = {
    type: -1, // 0 - tokens\ 1 - errors on lexer/ 2 - errors on syntax/ -1 -no choosed
    output: {
        LexerAnalysPart: {
            Tokens: null,
            Errors: null,
        },
        SyntaxAnalysPart: {
            Warnings: null,
            Errors: null,
            repair: null,
        },
    }
}

export default function OSI_outputState(state = initState, action){
    const {type, payload} = action;

    switch(type){
        case ADD_OUTPUT_BODY:
            let setupPayload = ExractDataToPayload(checkNullableObjects(SetArray(payload)))

            return {...state, output: setupPayload}        
        case CHANGE_OUTPUT_TYPE:
            return {...state, type: Number(payload)}
        default:
            return state;
    }
}

function ExractDataToPayload(data){
    let object = {
        LexerAnalysPart: {
            Tokens: data[0],
            Errors: data[1],
        },
        SyntaxAnalysPart:{
            Warnings: data[2],
            Errors: data[3],
            repair: data[4],
        }
    }
    return object
}

function SetArray(payload){
    let test = []

    test.push(payload.LexerAnalysPart.Tokens)
    test.push(payload.LexerAnalysPart.Errors)
    test.push(payload.SyntaxAnalysPart.Warnings)
    test.push(payload.SyntaxAnalysPart.Errors)
    test.push(payload.SyntaxAnalysPart.repair)

    return test
}

function checkNullableObjects(tests){
    let setupArr = []
    for(let i = 0; i < tests.length; i++){
        if (tests[i] === null){
            setupArr.push({})
        } else {
            setupArr.push(tests[i])
        }
    }
    return setupArr
}