const prefix = "OUTPUT_STATE_";

export const CHANGE_OUTPUT_TYPE = prefix + "CHANGE_OUTPUT_TYPE";
export const ADD_OUTPUT_BODY = prefix + "ADD_OUTPUT_BODY";