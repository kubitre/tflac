const prefix = "INPUT_STATE_";

export const CHANGE_SOURCE_TEXT = prefix + "CHANGE_SOURCES";
export const ADD_TO_CASH = prefix + "ADD_TO_CASH";
export const CHANGE_CONTEXT = prefix + "CHANGE_CONTEXT";