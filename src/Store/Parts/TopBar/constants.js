const prefix = "TOPBAR_STATE_";

export const ADD_NEW_FILE = prefix + "ADD_NEW_FILE";
export const CLOSE_FILE = prefix + "CLOSE_FILE";
export const SET_ACTIVE_TAB = prefix + "SET_ACTIVE_TAB";