export const GET_USER = "GET_USER_BY_TOKEN";
export const CREATE_NEW_USER = "CREATE_NEW_USER";
export const GET_DIR_USER = "GET_DIRECTORY_USER";
export const LOGOUT_USER = "LOGOUT_USER";