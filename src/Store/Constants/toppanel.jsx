const prefix = "TOP_PANEL_";

export const SET_FILE_ACTIVE = prefix + "SET_FILE_ACTIVE";
export const CLOSE_FILE = prefix + "CLOSE_FILE";
export const NEW_FILE = prefix + "NEW_FILE";