### This project is frontend of compiler on golang and merge Fortran
Our application contain text editor, now this text editor is not stable version, and also contain syntax higliter
### For start our application you should do this points:

###1) installation npm dependecies needs for our application, such as following command: 
```
npm install
```

###2) run and build main kernel of backend service such as following command: 

```
go build ./backend/app.go - for building binary file
./backend/app - for starting service
```

###3) run the client application following command: 
in main folder frontend:
```
npm start | or with used yarn package manager: yarn start
```